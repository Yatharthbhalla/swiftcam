//
//  ViewController.swift
//  swiftCam
//
//  Created by Yatharth Bhalla on 9/8/15.
//  Copyright (c) 2015 Clicklabs143. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var myImageview: UIImageView!
let picker = UIImagePickerController()

    //MARK: - Actions
@IBAction func shootPhoto(sender: UIBarButtonItem){
    if UIImagePickerController.availableCaptureModesForCameraDevice(.Rear) != nil {
        
    
    picker.allowsEditing = false
    picker.sourceType = UIImagePickerControllerSourceType.Camera
    picker.modalPresentationStyle = .FullScreen
    presentViewController(picker, animated: true, completion: nil)
    }
    else
    {
       noCamera()
    }
    }
   
    @IBOutlet weak var navigate: UINavigationBar!
    
@IBAction func photofromLibrary(sender: UIBarButtonItem) {
    picker.allowsEditing = false
    picker.sourceType = .PhotoLibrary
    picker.modalPresentationStyle = .Popover
    presentViewController(picker, animated: true, completion: nil)
picker.popoverPresentationController?.barButtonItem = sender
    
    }
    
    
    @IBAction func handlePinch(recognizer : UIPinchGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = CGAffineTransformScale(view.transform,
                recognizer.scale, recognizer.scale)
            recognizer.scale = 1
        }
    }
    
    @IBAction func handleRotate(recognizer : UIRotationGestureRecognizer) {
        if let view = recognizer.view {
            view.transform = CGAffineTransformRotate(view.transform, recognizer.rotation)
            recognizer.rotation = 0
        }
    }
    
    @IBAction func handlePan(recognizer:UIPanGestureRecognizer) {
        let translation = recognizer.translationInView(self.view)
        if let view = recognizer.view {
            view.center = CGPoint(x:view.center.x + translation.x,
                y:view.center.y + translation.y)
        }
        recognizer.setTranslation(CGPointZero, inView: self.view)
    }
    
    // MARK: - Methods
    func noCamera()
    {
        let alertVC = UIAlertController(title: "No CAMERA", message: "Sorry, this device has no camera", preferredStyle: .Alert)
        let okAction = UIAlertAction(title: "OK", style: .Default, handler: nil)
        alertVC.addAction(okAction)
        presentViewController(alertVC, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    //MARK: Delegates
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        var choosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        myImageview.image = choosenImage
        dismissViewControllerAnimated(true, completion: nil)
        println(info)
       
    }
        //func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
    //}
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
     dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

